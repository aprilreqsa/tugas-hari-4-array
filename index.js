var funcLib = require('./lib/funcLib')
var range = funcLib.range;
var rangeWithStep = funcLib.rangeWithStep;
var sum = funcLib.sum;
var dataHandling = funcLib.dataHandling;
var balikKata = funcLib.balikKata;
var agrs = process.argv;
switch(agrs[2]) {
    case "range": 
    var  startNum = agrs[3];   
    var  finishNum = agrs[4];
     console.log(range(startNum,finishNum));
    break;
    case "rangeWithStep" :
    var  startNum = agrs[3];   
    var  finishNum = agrs[4];
    var step        = agrs[5];
    console.log(rangeWithStep(startNum,finishNum,step));
    break;
    case "sum" :
    var  startNum = agrs[3];   
    var  finishNum = agrs[4];
    var step        = agrs[5];
    console.log(sum(startNum,finishNum,step));
    break;
    case "dataHandling" :
    dataHandling();
    break;
    case "balikKata" :
    var kata = agrs[3];
    balikKata(kata);
    break;
    default : console.log("perintah yang anda masukan salah!")
}
